package com.inkglobal.weatherdemo.app.objects;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ferran on 13/03/14.
 */
public class Weather {
    private String main;
    private String description;
    private String icon;

    public Weather(JSONObject jsonObject) throws JSONException {
        this.main = jsonObject.getString("main");
        this.description = jsonObject.getString("description");
        this.icon = "http://openweathermap.org/img/w/"+jsonObject.getString("icon")+".png";
    }

    public String getMain() {
        return main;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }
}
