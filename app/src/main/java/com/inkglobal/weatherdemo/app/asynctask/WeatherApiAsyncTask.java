package com.inkglobal.weatherdemo.app.asynctask;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inkglobal.weatherdemo.app.R;
import com.inkglobal.weatherdemo.app.communication.ApiConnector;
import com.inkglobal.weatherdemo.app.objects.CityWeather;
import com.inkglobal.weatherdemo.app.objects.Weather;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * Created by ferran on 13/03/14.
 */
public class WeatherApiAsyncTask extends AsyncTask<String, Void, CityWeather> {
    private Context context;

    public WeatherApiAsyncTask(Context context){
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        showLoading(View.VISIBLE);
        showContent(View.GONE);
    }

    private void showContent(int visible) {
        ((Activity) context).findViewById(R.id.container).setVisibility(visible);
    }

    private void showLoading(int visible) {
        ((Activity) context).findViewById(R.id.progressBar).setVisibility(visible);
    }

    protected CityWeather doInBackground(String... args) {
        String city = args[0];
        String weatherInfo = new ApiConnector().getWeatherByCity(city);

        try {
            return new CityWeather(weatherInfo);
        } catch (JSONException e) {
            Log.e("INK_DEMO", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }


    protected void onPostExecute(CityWeather cityWeather) {
        showLoading(View.GONE);
        if(cityWeather != null){
           showInformation(cityWeather);
        }else{
            Toast.makeText(context, context.getResources().getString(R.string.error_get_weather), Toast.LENGTH_LONG).show();
        }
    }

    private void showInformation(CityWeather cityWeather) {
        setTextViewValue(R.id.cityName, getStringResource(R.string.name)+": "+cityWeather.getName());
        setTextViewValue(R.id.longitudeLatitude, getStringResource(R.string.location)+": "+cityWeather.getLongitude()+" - "+cityWeather.getLatitude());
        setTextViewValue(R.id.temperature, getStringResource(R.string.temperature)+": "+cityWeather.getTemperate()+" ("+cityWeather.getTemperatureRange()+")");
        setTextViewValue(R.id.pressure, getStringResource(R.string.pressure)+": "+cityWeather.getPressure());
        setTextViewValue(R.id.humidity, getStringResource(R.string.humidity)+": "+cityWeather.getHumidity());

        setDifferentWeatherUi(cityWeather.getListWeather());
        showContent(View.VISIBLE);
    }

    private void setDifferentWeatherUi(ArrayList<Weather> listWeather) {

        LinearLayout listWeatherLayout = (LinearLayout) ((Activity) context).findViewById(R.id.weatherList);
        listWeatherLayout.removeAllViews();

        Iterator<Weather> it = listWeather.iterator();
        while(it.hasNext())
        {

            View child = ((Activity) context).getLayoutInflater().inflate(R.layout.weather_child, null);

            Weather obj = it.next();
            ((TextView) child.findViewById(R.id.nameWeather)).setText(obj.getMain());
            ((TextView) child.findViewById(R.id.descriptionWeather)).setText(obj.getDescription());
            ImageView icon = (ImageView) child.findViewById(R.id.icon);
            new DownloadImageAsyncTask(icon).execute(obj.getIcon());

            listWeatherLayout.addView(child);
        }
    }

    private void setTextViewValue(int textViewId, String value) {
        ((TextView) ((Activity) context).findViewById(textViewId)).setText(value);
    }

    private String getStringResource(int resourceId){
        return context.getResources().getString(resourceId);
    }
}
