package com.inkglobal.weatherdemo.app.objects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ferran on 13/03/14.
 */
public class CityWeather {

    private String name;
    private String longitude;
    private String latitude;
    private ArrayList<Weather> listWeather;
    private String temperate;
    private String temperatureRange;
    private String pressure;
    private String humidity;

    public CityWeather(String weatherInfo) throws JSONException {
        JSONObject jsonWeatherInfo = new JSONObject(weatherInfo);

        addDifferentWeathers(jsonWeatherInfo.getJSONArray("weather"));

        this.name = jsonWeatherInfo.getString("name");
        this.longitude = jsonWeatherInfo.getJSONObject("coord").getString("lon");
        this.latitude = jsonWeatherInfo.getJSONObject("coord").getString("lat");
        this.temperate = jsonWeatherInfo.getJSONObject("main").getString("temp");
        this.temperatureRange = jsonWeatherInfo.getJSONObject("main").getString("temp_min")+" - "+jsonWeatherInfo.getJSONObject("main").getString("temp_max");
        this.pressure =  jsonWeatherInfo.getJSONObject("main").getString("pressure");
        this.humidity =  jsonWeatherInfo.getJSONObject("main").getString("humidity");
    }

    private void addDifferentWeathers(JSONArray weather) throws JSONException {
        this.listWeather = new ArrayList<Weather>();
        for (int i=0; i < weather.length();i++){
            listWeather.add(
                    new Weather(weather.getJSONObject(i))
            );
        }
    }

    public String getName() {
        return name;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public ArrayList<Weather> getListWeather() {
        return listWeather;
    }

    public String getTemperate() {
        return temperate;
    }

    public String getTemperatureRange() {
        return temperatureRange;
    }

    public String getPressure() {
        return pressure;
    }

    public String getHumidity() {
        return humidity;
    }
}
