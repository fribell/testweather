package com.inkglobal.weatherdemo.app.communication;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

/**
 * Created by ferran on 13/03/14.
 */
public class ApiConnector {
    private String TAG = "API CONNECTION";
    private int TIMEOUT_CONNECTION = 5000;
    private int TIMEOUT_SOCKET = 5000;
    private int RESPONSE_200 = 200;

    /**
     * Send request to get the weather information
     * @param city
     * @return String
     */
    public String getWeatherByCity(String city){
        HttpClient httpClient = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), TIMEOUT_CONNECTION);
        HttpConnectionParams.setSoTimeout(httpClient.getParams(), TIMEOUT_SOCKET);

        HttpGet requestGet = new HttpGet("http://api.openweathermap.org/data/2.5/weather?q="+city+",uk");
        InputStream is = null;
        try {
            HttpResponse response = httpClient.execute(requestGet);

            if (response.getStatusLine().getStatusCode() != RESPONSE_200 ) {
                return null;
            }

            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            return readIt(is);

        } catch (ConnectTimeoutException e) {
            Log.e(TAG, "Timeout error: " + e.getMessage());
        } catch (SocketTimeoutException e) {
            Log.e(TAG, "Data retrieval or connection timed out");
        } catch (IOException e) {
            Log.e(TAG, "Server error: " + e.getMessage());
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    Log.e(TAG, "Closing inputStream " + e.getMessage());
                }
            }
        }

        return null;
    }

    /**
     * Reads an InputStream and converts it to a String.
     * @param stream
     * @return String
     * @throws IOException
     */
    private String readIt(InputStream stream) throws IOException {
        StringBuilder content = new StringBuilder(stream.available());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            content.append(line);
        }
        bufferedReader.close();

        return content.toString();
    }

    /**
     * Check if we have connection in the device
     * @param context
     * @return boolean
     */
    public boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
